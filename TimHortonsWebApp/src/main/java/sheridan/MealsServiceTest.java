package sheridan;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

public class MealsServiceTest {

	
	public void testDrinksRegular() {
		MealsService mealService = new MealsService();
		 List<String> types  = mealService.getAvailableMealTypes(MealType.DRINKS);
		 assertTrue(types != null);
	}

	public void testDrinksException() {
		MealsService mealService = new MealsService();
		 List<String> types  = mealService.getAvailableMealTypes(null);
		 assertTrue(types.get(0).equals("No Brand Available"));
	}
	@Test
	public void testDrinksBoundaryIn() {
		MealsService mealService = new MealsService();
		 List<String> types  = mealService.getAvailableMealTypes(MealType.DRINKS);
		 assertTrue(types.size() >= 3);
	}
	
	@Test
	public void testDrinksBoundaryOut() {
		MealsService mealService = new MealsService();
		 List<String> types  = mealService.getAvailableMealTypes(null);
		 assertTrue(types.size() <= 1);
	}

}
